(function ($) {

    "use strict";

    angular.module('app')
            .filter('coordFilter', coordFilter)
            .filter('tpcodigoFilter', tpcodigoFilter)
            .filter('pcodigoFilter', pcodigoFilter);

    /*@ngInject*/
    function coordFilter() {
        return function (text, split) {

            var splitter = split || '|';

            if (text != null && typeof text != undefined) {

                var splitterText = text.split(splitter);
                var joinSplitter = splitterText.join(',');
                return joinSplitter;
            } else {
                return text;
            }
        }
    }

    /*@ngInject*/
    function tpcodigoFilter() {

        return function (inputArray, searchCriteria) {

            var criteria = ['PROMO', 'NOVED', 'PRODU', 'SERVI', 'INFOR'];

            if (angular.isDefined(searchCriteria) && searchCriteria != '')
                criteria = searchCriteria;

            var data = [];
            angular.forEach(inputArray, function (item) {
                if (criteria.indexOf(item.codigo) !== -1)
                    data.push(item);
            });
            return data;
        };
    }
    
    /*@ngInject*/
    function pcodigoFilter() {

        return function (inputArray, searchCriteria) {

            var criteria = ['PROMO', 'NOVED', 'PRODU', 'SERVI', 'INFOR'];

            if (angular.isDefined(searchCriteria) && searchCriteria != '')
                criteria = searchCriteria;

            var data = [];
            angular.forEach(inputArray, function (item) {
                if (criteria.indexOf(item.tipoPublicacionDto.codigo) !== -1)
                    data.push(item);
            });
            return data;
        };
    }    

})(jQuery);    
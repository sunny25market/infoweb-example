(function ($) {

    "use strict";

    angular.module('app.category', []);

    //controller
    angular.module('app.category')
            .controller('CategoryLayoutController', CategoryLayoutController)
            .controller('CategoryListController', CategoryListController);


    /*@ngInject*/
    function CategoryLayoutController($scope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        function init() {

        }
    }

    /*@ngInject*/
    function CategoryListController($scope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.data = {
            grupo_categoria: [],
            categoria: []
        };

        vm.fn = {
            haveCategoria: function (grupo) {
                var catLength = vm.data.categoria.length;
                for (var i = 0; i < catLength; i++) {
                    if (vm.data.categoria[i]['grupoCategoriaDto']['id'] === grupo.id)
                        return true;
                }
                return false;
            },
        }

        vm.init = init;

        vm.init();

        function init() {
            var grupo_categoria = AppNomenclatorSvc.getNomenclador('GRUPO_CATEGORIA');
            grupo_categoria
                    .then(function (data) {
                        vm.data.grupo_categoria = data;
                        return $q(function (resolve, reject) {
                            resolve(data);
                        })
                    })
                    .then(function (grupo_cat) {
                        CategoriaSvc.query().then(function (data) {
                            vm.data.categoria = data;
                        });
                    });
        }
    }

})(jQuery);


(function ($) {

    "use strict";

    angular.module('app.site', []);

    //controller
    angular.module('app.site')
            .controller('SiteController', SiteController)
            .controller('SiteCategoriasController', SiteCategoriasController);

    /*@ngInject*/
    function SiteController($scope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        function init() {

        }
    }

    /*@ngInject*/
    function SiteCategoriasController($scope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        function init() {

        }
    }

})(jQuery);


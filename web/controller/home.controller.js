(function ($) {

    "use strict";

    angular.module('app.home', []);

    angular.module('app.home')
            .controller('HomeController', HomeController)
            .controller('TopBgController', TopBgController)
            .controller('HeaderTopController', HeaderTopController)
            .controller('MegaNavController', MegaNavController)
            .controller('BannerController', BannerController)
            .controller('MapPlacesController', MapPlacesController)
            .controller('CateController', CateController)
            .controller('CateBottomController', CateBottomController)
            .controller('WellcomeController', WellcomeController)
            .controller('BannerBottom1Controller', BannerBottom1Controller)
            .controller('BannerBottomController', BannerBottomController)
            .controller('FooterController', FooterController)
            .controller('CategoriasController', CategoriasController)
            .controller('NovedadesController', NovedadesController)
            .controller('PromocionesController', PromocionesController)
            .controller('LoginformController', LoginformController);

    angular.module('app.home')
            .directive('mcuiMegaMenu', McuiMegaMenu)
            .directive('mcuiBannerSlide', McuiBannerSlide)
            .directive('mcuiBannerImage', McuiBannerImage);

    /*@ngInject*/
    function HomeController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function LoginformController(AuthSvc, localStorageService, APP_UAUTH, $rootScope) {

        var vm = this;

        vm.init = init;

        vm.fn = {
            login: login
        };

        vm.init();

        function init() {

        }

        function login(valid, username, password) {

            if (!valid || !username || !password)
                return;

            var data = {username: username, password: password};

            AuthSvc.attemptLogin(data).then(function (response) {

                var auth = response;

                var uAuthenticated = {
                    uid: auth.id,
                    username: auth.username,
                    email: auth.email,
                    rol: auth.tipoUsuarioDto
                };

                localStorageService.set('bearer', auth.tokenAuth);
                localStorageService.set(APP_UAUTH.key, uAuthenticated);
                $rootScope.$appUser = uAuthenticated;

                $rootScope.$broadcast('auth:login', uAuthenticated);

            }, function (reason) {
                alert("Usuario o contraseña inválido");
            });
        }
    }

    /*@ngInject*/
    function TopBgController($modal, AuthSvc, $rootScope, $state) {

        var vm = this;

        vm.init = init;

        vm.loginModal = $modal({templateUrl: 'web/views/components/login.modal.html', show: false, title: 'Autenticar'});

        vm.fn = {
            goToLogin: goToLogin,
            closeModalLogin: closeModalLogin,
            goToMyAccount: goToMyAccount,
            goToLogout: goToLogout
        };

        $rootScope.$on('auth:login', function (evt, data) {
            if (data && data != null)
                vm.fn.closeModalLogin();
        });

        vm.init();

        function init() {

        }

        function goToLogin() {
            vm.loginModal.$promise.then(vm.loginModal.show);
        }

        function closeModalLogin() {
            vm.loginModal.$promise.then(vm.loginModal.hide);
        }

        function goToMyAccount() {
            $state.go('account.view');
        }

        function goToLogout() {
            AuthSvc.logout().then(function (response) {
                $rootScope.$appUser = null;
                $state.go('home');
            }, function (err) {

            })
        }
    }

    /*@ngInject*/
    function HeaderTopController(PublicacionSvc) {

        var vm = this;

        vm.init = init;

        vm.collection = [];

        vm.init();

        function init() {
            var qry_publicaciones = PublicacionSvc.findByCodigo('WBSPR');
            qry_publicaciones.then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log("ERROR: ", err);
            });
        }
    }

    /*@ngInject*/
    function MegaNavController(AppNomenclatorSvc, CategoriaSvc, $q, $filter, $rootScope, $state) {

        var vm = this;

        vm.init = init;

        vm.init();

        vm.grupo_categoria = [];
        vm.categoria = [];

        vm.fn = {
            haveCategoria: function (grupo) {
                var catLength = vm.categoria.length;
                for (var i = 0; i < catLength; i++) {
                    if (vm.categoria[i]['grupoCategoriaDto']['id'] === grupo.id)
                        return true;
                }
                return false;
            },
            searchByText: function (text) {
                var statename = $state.current.name;
                if (text && text != null && text != '') {
                    if (statename === 'product.searchbysubcategory' || statename === 'product.searchbytext') {
                        $rootScope.$broadcast('post:filterby:text', text);
                    } else {
                        $state.go('product.searchbytext', {text: text});
                    }
                }
            },
            searchByCategory: function (id) {
                var statename = $state.current.name;
                if (id) {
                    if (statename === 'product.searchbysubcategory' || statename === 'product.searchbytext') {
                        $rootScope.$broadcast('post:filterby:subcategory', id);
                    } else {
                        $state.go('product.searchbysubcategory', {id: id});
                    }
                }
            }
        }

        function init() {

            var grupo_categoria = AppNomenclatorSvc.getNomenclador('GRUPO_CATEGORIA');
            grupo_categoria
                    .then(function (data) {
                        vm.grupo_categoria = data;
                        return $q(function (resolve, reject) {
                            resolve(data);
                        })
                    })
                    .then(function (grupo_cat) {
                        CategoriaSvc.query().then(function (data) {
                            vm.categoria = data;
                        });
                    });
        }
    }

    /*@ngInject*/
    function BannerController(PublicacionSvc) {

        var vm = this;

        vm.collection = [];

        vm.init = init;

        vm.init();

        function init() {
            var qry_publicaciones = PublicacionSvc.findByCodigo('WSLPR');
            qry_publicaciones.then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log("ERROR: ", err);
            });
        }
    }

    /*@ngInject*/
    function MapPlacesController(PublicacionSvc, $q, $timeout) {

        var vm = this;

        vm.collection = [];

        vm.init = init;

        vm.fn = {
            addMarker: addMarker
        };

        vm.data = {
            coords: ['-25.288987,-57.654484', '-25.283177,-57.636213', '-25.271587,-57.605223']
        }

        vm.init();

        function init() {

        }

        function addMarker(event) {
            /*var ll = event.latLng;
             vm.coord = ll.lat() + ',' + ll.lng();
             console.log(vm.coord);*/
        }
    }

    /*@ngInject*/
    function CateController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function CateBottomController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function WellcomeController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function BannerBottom1Controller() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function BannerBottomController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }

    /*@ngInject*/
    function FooterController() {

        var vm = this;

        vm.init = init;

        vm.init();

        function init() {

        }
    }


    /*------ DIRECTIVES -----*/

    /*@ngInject*/
    function McuiMegaMenu($timeout) {
        return {
            restrict: 'EA',
            scope: false,
            link: function (scope, element, attrs) {
                $timeout(function () {
                    $(element).megamenu();
                });
            }
        };
    }

    /*@ngInject*/
    function McuiBannerSlide($timeout) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs) {

            }
        };
    }


    /*@ngInject*/
    function McuiBannerImage($timeout) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs) {
                var url = attrs.mcuiBannerImage || "../images/banner.jpg";
                $timeout(function () {
                    $(element).css({background: 'url(' + url + ') no-repeat center center'});
                    console.log("URL:", url);
                });

            }
        };
    }

    /*@ngInject*/
    function CategoriasController($scope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        vm.data = {
            subcategoria: []
        };

        vm.form = {
            subcategoria: {}
        };

        vm.display = {
            search: false
        }

        vm.fn = {
            filterByOneSubcategoria: function (cat) {
                $state.go('product.searchbysubcategory', {id: cat.id});
            }
        };

        vm.init();

        function init() {
            //
            //SUB-CATEGORIAS
            CategoriaSvc.query().then(function (data) {
                vm.data.subcategoria = data;
            });

        }
    }

    /*@ngInject*/
    function NovedadesController(PublicacionSvc) {

        var vm = this;

        vm.init = init;

        vm.collection = [];

        vm.init();

        function init() {
            var qry_publicaciones = PublicacionSvc.findByCodigo('NOVED');
            qry_publicaciones.then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log("ERROR: ", err);
            });
        }
    }

    /*@ngInject*/
    function PromocionesController(PublicacionSvc) {

        var vm = this;

        vm.init = init;

        vm.collection = [];

        vm.init();

        function init() {
            var qry_publicaciones = PublicacionSvc.findByCodigo('PROMO');
            qry_publicaciones.then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log("ERROR: ", err);
            });
        }
    }

})(jQuery);


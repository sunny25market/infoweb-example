(function ($) {

    "use strict";

    angular.module('app.account', []);

    //controller
    angular.module('app.account')
            .controller('AccountLayoutController', AccountLayoutController)
            .controller('AccountViewController', AccountViewController)
            .controller('AccountEditController', AccountEditController)
            .controller('FavouritesController', FavouritesController)
            .controller('WishesController', WishesController)
            .directive('mcuiMylist', McuiMylist);


    /*@ngInject*/
    function AccountLayoutController($scope, AccountSvc, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        function init() {

        }
    }

    /*@ngInject*/
    function AccountViewController($scope, AccountSvc, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        vm.model = {};

        vm.init();

        function init() {
            AccountSvc.get().then(function (data) {
                vm.model = data;
            }, function (err) {
                console.log(err);
            })
        }

        $scope.tabs = [
            {
                "title": "Home",
                "content": "Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica."
            },
            {
                "title": "Profile",
                "content": "Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee."
            },
            {
                "title": "About",
                "content": "Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.",
                "disabled": true
            }
        ];
        $scope.tabs.activeTab = "Profile";
    }

    /*@ngInject*/
    function AccountEditController($scope, AccountSvc, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state) {

        var vm = this;

        vm.init = init;

        vm.model = {};

        vm.fn = {
            saveModel: saveModel
        }

        vm.init();

        function init() {
            AccountSvc.get().then(function (data) {
                vm.model = data;
            }, function (err) {
                console.log(err);
            })
        }

        function saveModel(isValid) {
            if (!isValid) {
                return;
            }

            var model = angular.copy(vm.model);

            delete model['avatar'];            

            AccountSvc.update(model).then(function (data) {
                if (vm.file != null) {
                    AccountSvc.uploadImage(vm.file, data.id).then(function (file) {
                    }, function (err) {
                        console.log("Ha ocurrido un error intentando guardar la imagen. ", err);
                    }).finally(function () {
                        $state.go('account.view');
                    });
                } else {
                    $state.go('account.view');
                }

            }, function (err) {
                alert(err);
            });
        }
    }

    /*@ngInject*/
    function FavouritesController(AccountSvc) {
        var vm = this;

        vm.init = init;

        vm.collection = [];

        vm.fn = {};

        vm.init();

        function init() {
            AccountSvc.myFavourites().then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log(err);
            })
        }
    }

    /*@ngInject*/
    function WishesController(AccountSvc) {

        var vm = this;

        vm.init = init;

        vm.collection = [];

        vm.fn = {};

        vm.init();

        function init() {
            AccountSvc.myWishes().then(function (data) {
                vm.collection = data;
            }, function (err) {
                console.log(err);
            })
        }
    }

    /*@ngInject*/
    function McuiMylist() {

        /*@ngInject*/
        function MyListController() {

        }

        var directive = {
            restrict: 'EA',
            templateUrl: 'web/views/account/partials/mylist.tmpl.html',
            link: function (scope, element, attrs, ctrl) {
                scope.editMode = attrs.editMode ? true : false;
            },
            controller: MyListController,
            controllerAs: 'vm',
            scope: {
                collection: "=",
                noResult: "@",
                type: "@"
            }
        };

        return directive;
    }

})(jQuery);


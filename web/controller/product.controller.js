(function ($) {

    "use strict";

    angular.module('app.product', []);

    //controller
    angular.module('app.product')
            .controller('ProductController', ProductController)
            .controller('ProductViewController', ProductViewController);
    //service
    angular.module('app.product')
            .service('ProductSvc', ProductSvc);


    /*@ngInject*/
    function ProductController($scope, $rootScope, AppNomenclatorSvc, CategoriaSvc, $q, $filter, ProductSvc, ClienteSvc, $location, $state, PublicacionSvc) {

        var vm = this;

        vm.init = init;

        vm.data = {
            categoria: [],
            subcategoria: [],
            subcategoria_copy: [],
            productos: [],
            productos_copy: [],
            clientes: [],
            recomendados: []
        };

        vm.moreResult = {};

        vm.form = {
            subcategoria: {}
        };

        vm.display = {
            search: false
        }

        vm.viewmode = {
            grid: true
        }

        $scope.$on('post:filterby:subcategory', function (event, id) {

            var categoryID = id;

            if (categoryID != null) {
                var filter = {};
                filter[categoryID] = true;
                vm.form.subcategoria = filter;
                var categoriaFound = $filter('filter')(vm.data.subcategoria, {id: categoryID});
                if (categoriaFound.length > 0) {
                    //console.log('post:filterby:subcategory:', categoriaFound[0]);
                    vm.fn.filterByOneSubcategoriaRemote(categoriaFound[0]);
                    $state.go('product.searchbysubcategory', {id: categoryID});
                } else {
                    console.log('Categoría seleccionada no válida en listado de categorías de busqueda...');
                }
            }
            //console.log('post:filterby:subcategory=>', categoryID);
        });

        $scope.$on('post:filterby:text', function (evt, text) {
            angular.forEach(vm.data.subcategoria, function (item, i) {
                vm.form.subcategoria[item.id] = false;
            });
            vm.fn.searchProduct({query: text, reset: true});
            $state.go('product.searchbytext', {text: text});
        });

        vm.fn = {
            setGridViewMode: function (val) {
                vm.viewmode.grid = val;
            },
            selectCategoria: function (item) {
                var list = $filter('filter')(vm.data.subcategoria_copy, {grupoCategoriaDto: {id: item.id}});
                vm.data.subcategoria = angular.copy(list);
            },
            selectCliente: function (item) {
                var productos = angular.copy(vm.data.productos_copy);
                if (item != null && item != '') {
                    var r = [];
                    for (var i = 0; i < productos.length; i++) {
                        var e = productos[i];
                        if (e.sucursalDto.clienteDto.id == item.id)
                            r.push(e);
                    }
                    productos = r;
                }

                vm.data.productos = productos;
            },
            filterBySubcategoria: filterBySubcategoria,
            filterByOneSubcategoria: filterByOneSubcategoria,
            filterByOneSubcategoriaRemote: filterByOneSubcategoriaRemote,
            searchProduct: searchProduct,
            loadMore: loadMore,
            checkCategory: checkCategory
        };

        vm.init();

        function init() {

            var statename = $state.current.name;
            var allowState = ['product.searchbysubcategory', 'product.searchbytext', 'product.search'];
            if (allowState.indexOf(statename) === -1){
                
            }
            
            AppNomenclatorSvc.getNomenclador('GRUPO_CATEGORIA').then(function (data) {
                vm.data.categoria = data;
            }, function (err) {
                console.log(err);
            });

            CategoriaSvc.query().then(function (data) {
                vm.data.subcategoria = data;
                vm.data.subcategoria_copy = data;
            });

            //
            //CLIENTES
            ClienteSvc.query().then(function (data) {
                vm.data.clientes = data;
            }, function (err) {
                console.log(err);
            });

            //
            //RECOMENDADOS
            var qry_publicaciones = PublicacionSvc.findByCodigo('WRECO');
            qry_publicaciones.then(function (data) {
                vm.data.recomendados = data;
            }, function (err) {
                console.log("ERROR: ", err);
            });

            console.log('calling searchProduct');
            vm.fn.searchProduct();
        }

        //
        //{reset: true|false, categoryId: int, query: string, page: int, idCiudad: int}  
        function searchProduct(qry) {

            var locationUrl = $location.absUrl();
            var locationSplitted = locationUrl.split('/');
            var statename = $state.current.name;
            var reset = qry && typeof qry.reset != 'undefined' ? qry.reset : false;
            //
            //CATEGORIA
            var subcategoryid = qry && qry.categoryId ? qry.categoryId : null;
            //
            //TEXT
            var text = qry && qry.query ? qry.query : null;

            if (statename == 'product.searchbysubcategory' && subcategoryid == null && text == null) {
                subcategoryid = locationSplitted[locationSplitted.length - 1];
            }

            if (statename == 'product.searchbytext' && text == null && subcategoryid == null) {
                text = locationSplitted[locationSplitted.length - 1];
            }

            //
            //PAGE
            var page = qry && qry.page ? qry.page : 0;

            //
            //idCiudad
            var idCiudad = qry && qry.idCiudad ? qry.idCiudad : null;

            //
            //PRODUCTOS
            vm.display.search = true;
            ProductSvc.search({query: text, categoryId: subcategoryid, page: page, idCiudad: idCiudad}).then(function (response) {

                //---------BUILD DATA
                var data = response.result;

                vm.moreResult = {
                    next: response.next,
                    page: response.page,
                    previous: response.previous < 0 ? 0 : response.previous,
                    more: data.length < 10 ? false : true
                };
                //--------------------

                //console.log('RESULT:', data);

                if (vm.data.productos_copy.length == 0 || reset) {
                    vm.data.productos = data;
                    vm.data.productos_copy = data;

                    //console.log('PRIMERA BUSQUEDA:', data);
                } else {

                    //console.log('ANTES:', vm.data.productos_copy.length);

                    var copy = angular.copy(vm.data.productos_copy);
                    for (var i = 0; i < data.length; i++)
                        copy.push(data[i]);

                    vm.data.productos = copy;
                    vm.data.productos_copy = copy;

                    //console.log('DESPUES:', vm.data.productos_copy.length);
                }

                if (subcategoryid != null) {
                    var filter = {};
                    filter[subcategoryid] = true;
                    vm.fn.filterBySubcategoria(filter, true);
                    vm.form.subcategoria = filter;
                }

            }, function (err) {

                console.log('Err. obteniendo productos/clientes:', err);

            }).finally(function () {
                vm.display.search = false;
            });
        }

        function loadMore() {
            console.log('currentPage:', vm.moreResult.page, ' nextPage:', vm.moreResult.next);
            vm.fn.searchProduct({page: vm.moreResult.next});
        }

        function filterBySubcategoria(sub, prestrict) {

            vm.display.search = true;

            var restrict = prestrict || false;
            var subcat = sub || null;

            var catFilter = (subcat && subcat != null && restrict) ? subcat : angular.copy(vm.form.subcategoria);
            var data = angular.copy(vm.data.productos_copy);

            var hasFilter = false;
            for (var prop in catFilter)
                if (catFilter[prop] == true)
                    hasFilter = true;

            var result = [];

            if (hasFilter) {
                for (var i = 0; i < data.length; i++) {

                    var obj = data[i];
                    var objCategorias = obj['categoriasDto'];
                    var hasCategoria = false;
                    for (var c = 0; c < objCategorias.length; c++) {
                        var categ = objCategorias[c];
                        if (angular.isDefined(catFilter[categ.id]) && catFilter[categ.id] == true)
                            hasCategoria = true;
                    }
                    if (hasCategoria)
                        result.push(obj);
                }
            } else {
                result = data;
            }

            vm.data.productos = result;

            vm.display.search = false;
        }

        function filterByOneSubcategoria(sub, prestrict) {

            //$state.go('product.searchbysubcategory', {id: sub.id});

            vm.display.search = true;

            var restrict = prestrict || false;
            var subcat = sub || null;

            angular.forEach(vm.data.subcategoria, function (item, i) {
                if (item.id != sub.id)
                    vm.form.subcategoria[item.id] = false;
            });

            var catFilter = (subcat && subcat != null && restrict) ? subcat : angular.copy(vm.form.subcategoria);
            var data = angular.copy(vm.data.productos_copy);

            var hasFilter = false;
            for (var prop in catFilter)
                if (catFilter[prop] == true)
                    hasFilter = true;

            var result = [];

            if (hasFilter) {
                for (var i = 0; i < data.length; i++) {

                    var obj = data[i];
                    var objCategorias = obj['categoriasDto'];
                    var hasCategoria = false;
                    for (var c = 0; c < objCategorias.length; c++) {
                        var categ = objCategorias[c];
                        if (angular.isDefined(catFilter[categ.id]) && catFilter[categ.id] == true)
                            hasCategoria = true;
                    }
                    if (hasCategoria)
                        result.push(obj);
                }
            } else {
                result = data;
            }

            vm.data.productos = result;
            vm.display.search = false;
        }

        function filterByOneSubcategoriaRemote(sub, prestrict) {

            //vm.display.search = true;

            var restrict = prestrict || false;
            var subcat = sub || null;

            angular.forEach(vm.data.subcategoria, function (item, i) {
                if (item.id != sub.id)
                    vm.form.subcategoria[item.id] = false;
            });

            var catFilter = (subcat && subcat != null && restrict) ? subcat : angular.copy(vm.form.subcategoria);

            var hasFilter = false;
            for (var prop in catFilter)
                if (catFilter[prop] == true)
                    hasFilter = true;

            if (hasFilter) {
                vm.fn.searchProduct({categoryId: subcat.id, reset: true});
            } else {
                vm.fn.searchProduct({reset: true});
            }
        }

        function checkCategory(category) {
            $state.go('product.searchbysubcategory', {id: category.id});
            vm.fn.filterByOneSubcategoriaRemote(category);
        }
    }

    /*@ngInject*/
    function ProductViewController($scope, $stateParams, $filter, $timeout, SucursalSvc, ClienteSvc, PublicacionSvc, $modal, ProductSvc, ValoracionSvc, AuthSvc) {

        var vm = this;

        vm.init = init;

        vm.data = {
            coordenadas: [],
            sucursales: [],
            publicaciones: [],
            publicaciones_copy: [],
            tipoPublicaciones: []
        };

        vm.form = {};

        vm.fn = {
            verDetalleSucursal: verDetalleSucursal,
            selectTipoPublicacion: selectTipoPublicacion,
            showComments: showComments,
            showReportar: showReportar
        };

        vm.sucursal = {};

        vm.cliente = {};

        //filtro para la busqueda publicaciones de cliente
        vm.filterPublicacion = {};

        vm.init();

        function init() {

            var clienteId = $stateParams.id;
            var sucursalId = $stateParams.idsucursal || null;

            //console.log($stateParams);

            //CLIENTE
            var qryCliente = ClienteSvc.get(clienteId);
            qryCliente.then(function (data) {
                vm.cliente = data;
            }, function (err) {
                console.log(err);
            });

            //SUCURSALES
            var qrySucursales = SucursalSvc.getByCliente(clienteId);
            qrySucursales.then(function (data) {
                //
                //sucursales
                vm.data.sucursales = data;
                //
                //coordenadas de todas las sucursales
                var coordenadas = [];
                angular.forEach(vm.data.sucursales, function (value, key) {
                    coordenadas.push(value['coordenadas']);
                });
                vm.data.coordenadas = coordenadas;

                if (sucursalId != null) {
                    var filtered = $filter('filter')(vm.data.sucursales, {id: sucursalId});
                    if (filtered.length > 0)
                        vm.sucursal = filtered[0];
                    else
                        vm.sucursal = vm.data.sucursales[0];
                } else {
                    vm.sucursal = vm.data.sucursales[0];
                }

                console.log('SUCURSAL:', vm.sucursal);

                //
                //imagen cliente .archivos[0].url
                /*var clienteImagen = null;
                 for (var i = 0; i < data.length; i++) {
                 var suc = data[i];
                 if (suc.archivos && suc.archivos.length > 0) {
                 clienteImagen = suc.archivos[0]['url'];
                 break;
                 }
                 }
                 $timeout(function () {
                 vm.cliente['imagen'] = clienteImagen;
                 });*/

            }, function (err) {
                console.log(err);
            });

            //PUBLICACIONES            
            var qryPublicaciones = PublicacionSvc.getByCliente(clienteId);
            qryPublicaciones.then(function (data) {
                var dataFiltered = $filter('pcodigoFilter')(data);
                vm.data.publicaciones = dataFiltered;
                vm.data.publicaciones_copy = dataFiltered;
            }, function (err) {
                console.log(err);
            });

            //TIPOS DE PUBLICACION
            var qryTipoPublicacion = PublicacionSvc.getTipoPublicacion();
            qryTipoPublicacion.then(function (data) {
                vm.data.tipoPublicaciones = data;
            }, function (err) {
                console.log(err);
            });
        }

        //FUNCTIONS
        function verDetalleSucursal(item, refresh) {
            var remoteRefresh = refresh != null ? true : false;

            if (!remoteRefresh) {
                var sucursal = $filter('filter')(vm.data.sucursales, {id: item.id});
                $timeout(function () {
                    vm.sucursal = sucursal.length > 0 ? sucursal[0] : {};
                    console.log(vm.sucursal);
                })
            } else {
                SucursalSvc.get(item.id).then(function (response) {
                    vm.sucursal = response;
                }, function (err) {
                    console.log(err);
                    alert('Error actualizando datos de la sucursal');
                })
            }
        }

        function selectTipoPublicacion(item) {
            var pubs = angular.copy(vm.data.publicaciones_copy);
            if (item != null && item != '')
                pubs = $filter('filter')(pubs, {tipoPublicacionDto: {id: item.id}});

            vm.filterPublicacion.tipo = item;
            vm.data.publicaciones = pubs;
        }

        function createComment(rating) {

            var scope = $scope.$new();

            scope.rating = rating;
            scope.fn = {
                sendComment: function (invalid, data) {

                    if (invalid)
                        return;

                    var comment = data.comment;
                    var rating = data.rating;

                    vm.comentarioModal.$promise.then(vm.comentarioModal.hide);
                }
            }

            vm.comentarioModal = $modal({
                scope: scope,
                templateUrl: 'web/views/components/comment.modal.html',
                show: false,
                title: 'Comentario'
            });

            vm.comentarioModal.$promise.then(vm.comentarioModal.show);
        }

        function showComments() {

            var scope = $scope.$new();

            scope.comments = [];
            scope.commentForm = {
                rating: 1,
                comment: ""
            };
            scope.fn = {
                sendComment: function (invalid, data) {

                    if (invalid)
                        return;

                    var user = AuthSvc.appUser();

                    var obj = {
                        clienteSucursalDto: {
                            id: vm.sucursal.id
                        },
                        sucursalValoracionesDetDto: [
                            {
                                usuarioDto: {id: user.uid},
                                puntaje: data.rating,
                                mensaje: data.comment
                            }
                        ]
                    };

                    //console.log(obj);

                    alert('Muchas gracias por su comentario y valoración.');
                    vm.comentarioListModal.$promise.then(vm.comentarioListModal.hide);

                    scope.commentForm = {
                        rating: 1,
                        comment: ""
                    };

                    ValoracionSvc.create(obj).then(function (response) {
                        //console.log(response);
                        vm.fn.verDetalleSucursal(vm.sucursal, true);
                    }, function (err) {
                        alert(err);
                    })

                }
            }

            //usuario, comentario, fecha, rating

            ValoracionSvc.findBySucursal(vm.sucursal.id).then(function (data) {

                scope.comments = data;

            }, function (err) {
                console.log(err);
            });

            vm.comentarioListModal = $modal({
                scope: scope,
                templateUrl: 'web/views/components/commentlist.modal.html',
                show: false,
                title: 'o'
            });

            vm.comentarioListModal.$promise.then(vm.comentarioListModal.show);
        }

        function showReportar() {

            var scope = $scope.$new();

            scope.reportForm = {
                comment: ""
            };
            scope.fn = {
                sendReport: function (invalid, data) {

                    if (invalid)
                        return;

                    scope.reportForm = {
                        comment: ""
                    };

                    alert('Muchas gracias por su comentario.');
                    vm.reportformModal.$promise.then(vm.reportformModal.hide);
                }
            }

            vm.reportformModal = $modal({
                scope: scope,
                templateUrl: 'web/views/components/reportform.modal.html',
                show: false,
                title: 'o'
            });

            vm.reportformModal.$promise.then(vm.reportformModal.show);
        }
    }

    /*@ngInject*/
    function ProductSvc($http, $q, $filter, ClienteSvc, ClienteCategoriaSvc, API_INFOGUIA) {

        var service = {
            query: function (params) {

                function promise(resolve, reject) {

                    ClienteSvc.query().then(function (response) {
                        var data = response;
                        return $q(function (res, rej) {
                            res(data);
                        })
                    }, function (data) {
                        reject(data);
                    }).then(function (productos) {

                        var promises = [];

                        angular.forEach(productos, function (value, index) {
                            promises.push(ClienteCategoriaSvc.getByClienteId(value.id));
                        });

                        $q.all(promises).then(function (result) {

                            var resultFormatted = [];
                            for (var i = 0; i < result.length; i++)
                                if (angular.isDefined(result[i]) && angular.isDefined(result[i][0]))
                                    resultFormatted.push(result[i][0]);

                            angular.forEach(productos, function (product, index) {

                                var categoriasDto = [];

                                for (var i = 0; i < resultFormatted.length; i++) {
                                    var obj = resultFormatted[i];
                                    if (obj['clienteDto']['id'] == product['id'])
                                        categoriasDto.push(obj['categoriaDto']);
                                }

                                //console.log(product.nombreCompleto, categoriasDto);
                                product['categorias'] = categoriasDto;
                            });

                            resolve(productos);

                        }, function (reason) {
                            reject(reason);
                        });
                    });
                }

                return $q(promise);


                /*function promise(resolve, reject) {
                 
                 var productoMock = 'data/Productos.json';
                 
                 $http.get(productoMock).then(function (response) {
                 var data = response.data;
                 data.map(function (item, index) {
                 item['imagen'] = 'web/assets/img/no-thumbnail.jpg';
                 });
                 resolve(data);
                 }, function (data) {
                 reject(data);
                 });
                 }
                 
                 return $q(promise);*/
            },
            search: function (params) {
                return ClienteSvc.search(params);
            },
            getComments: function (id) {

                function promise(resolve, reject) {

                    var url = 'data/Comentario.json';

                    var requestParams = {
                        method: "GET",
                        url: url
                    };

                    $http(requestParams).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de comentarios...");
                    }
                }

                return $q(promise);
            }
        };

        return service;
    }

})(jQuery);


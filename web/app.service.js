(function ($) {

    "use strict";
    angular.module('app')
            .service('AppGeoPositionSvc', AppGeoPositionSvc)
            .service('AppNomenclatorSvc', AppNomenclatorSvc)
            .service('AppFileSvc', AppFileSvc)
            .factory('AppSetting', AppSetting)
            .service('CategoriaSvc', CategoriaSvc)
            .service('PublicacionSvc', PublicacionSvc)
            .service('ClienteSvc', ClienteSvc)
            .service('SucursalSvc', SucursalSvc)
            .service('ClienteCategoriaSvc', ClienteCategoriaSvc)
            .service('AuthSvc', AuthSvc)
            .service('AccountSvc', AccountSvc)
            .service('ValoracionSvc', ValoracionSvc);

    /*@ngInject*/
    function AuthSvc($q, localStorageService, $http, API_INFOGUIA, APP_UAUTH) {

        var AUTHSERVICEBASE = {
            LOGIN: API_INFOGUIA + '/auth/admin/login'
        };

        var service = {
            attemptLogin: function (loginObject) {

                return $q(function (resolve, reject) {

                    var login = {
                        username: loginObject.username || "",
                        password: loginObject.password || "",
                        //rememberMe: loginObject.rememberMe || false
                    };

                    $http({
                        method: "POST",
                        url: AUTHSERVICEBASE.LOGIN,
                        data: login,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        if (response.data)
                            resolve(response.data);
                        else
                            reject(response.status);
                    }

                    function errorResponse(response) {
                        reject(response.status);
                    }
                })
            },
            logout: function () {
                return $q(function (resolve, reject) {
                    localStorageService.clearAll();
                    resolve(true);
                })
            },
            signup: function (data) {

            },
            isAuth: function () {
                var token = localStorageService.get('bearer');
                if (token)
                    return true;

                return false;
            },
            appUser: function () {
                return localStorageService.get(APP_UAUTH.key);
            },
            appUserFullName: function () {
                var u = localStorageService.get(APP_UAUTH.key);
                return u.nombres + ' ' + u.apellidos;
            }
        }

        return service;
    }

    /*@ngInject*/
    function AppGeoPositionSvc($q, $filter) {

        var service = {
            getCurrentPosition: function () {

                function promise(resolve, reject) {

                    var options = {enableHighAccuracy: true};

                    function geolocationSuccess(pos) {

                        var position = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

                        resolve({
                            data: pos,
                            coords: pos.coords,
                            coords_v1: pos.coords.latitude + ',' + pos.coords.longitude,
                            coords_v2: pos.coords.latitude + '|' + pos.coords.longitude,
                            latitude: pos.coords.latitude,
                            longitude: pos.coords.longitude
                        });
                    }

                    function geolocationError(error) {

                        var message = "Imposible obtener Geolocalización. ";

                        switch (error.code) {
                            case error.PERMISSION_DENIED:
                                message += "User denied the request for Geolocation. ";
                                break;
                            case error.POSITION_UNAVAILABLE:
                                message += "Location information is unavailable."
                                break;
                            case error.TIMEOUT:
                                message += "The request to get user location timed out."
                                break;
                            case error.UNKNOWN_ERROR:
                                message += "An unknown error occurred."
                                break;
                        }

                        reject(message + error.message);
                    }

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, options);
                    } else {
                        reject("Geolocation is not supported by this browser.");
                    }
                }

                return $q(promise);

            }
        };

        return service;
    }

    /*@ngInject*/
    function AppNomenclatorSvc($q, $filter, $http, API_INFOGUIA) {

        var service = {
            getNomenclador: function (pnomenclador, remote, id) {

                //------- NOMENCLADORES ----- 
                //  
                //  => TIPO_PUBLICACION 
                //  => ESTADO_PUBLICACION
                //  => TIPO_USUARIO
                //  => ESTADO_USUARIO
                //  => GRUPO_CATEGORIA

                var pid = id || null;
                var premote = (typeof remote == undefined || remote == null || remote == "") ? false : true;

                function NOMENCLADOR(model) {
                    this.id = model.id || null;
                    this.descripcion = model.descripcion || null;
                }

                var tipoPublicacion = [
                    new NOMENCLADOR({id: 1, descripcion: "Promociones"}),
                    new NOMENCLADOR({id: 2, descripcion: "Novedades"})
                ];

                var estadoPublicacion = [
                    new NOMENCLADOR({id: 1, descripcion: "ACTIVO"}),
                    new NOMENCLADOR({id: 2, descripcion: "INACTIVO"})
                ];

                var tipoUsuario = [
                    new NOMENCLADOR({id: 1, descripcion: "Administrador"}),
                    new NOMENCLADOR({id: 2, descripcion: "Usuario"}),
                    new NOMENCLADOR({id: 3, descripcion: "Cliente"})
                ];

                var estadoUsuario = [
                    new NOMENCLADOR({id: 1, descripcion: "Activo"}),
                    new NOMENCLADOR({id: 2, descripcion: "Inactivo"}),
                    new NOMENCLADOR({id: 3, descripcion: "Pendiente de activación"})
                ];

                var grupoCategoria = [
                    new NOMENCLADOR({id: 1, descripcion: "Informaciones"}),
                    new NOMENCLADOR({id: 2, descripcion: "Servicios"}),
                    new NOMENCLADOR({id: 3, descripcion: "Negocios"}),
                    new NOMENCLADOR({id: 8, descripcion: "Turismo & Ocio"})
                ];

                function promise(resolve, reject) {

                    var collection = [];

                    switch (pnomenclador) {
                        case 'TIPO_PUBLICACION':
                            collection = tipoPublicacion;
                            break;
                        case 'ESTADO_PUBLICACION':
                            collection = estadoPublicacion;
                            break;
                        case 'TIPO_USUARIO':
                            collection = tipoUsuario;
                            break;
                        case 'ESTADO_USUARIO':
                            collection = estadoUsuario;
                            break;
                        case 'GRUPO_CATEGORIA':
                            collection = grupoCategoria;
                            break;
                        default:
                            collection = [];
                    }

                    if (!pnomenclador) {
                        reject('Debe indicar el parámetro "pnomenclador" de la función');
                    }
                    else if (pid !== null) {
                        var result = $filter('filter')(collection, {id: pid});
                        if (result !== null && result.length > 0)
                            resolve(result[0]);
                        else
                            reject("No se encuentra " + pnomenclador + " => ID:" + pid);
                    } else {
                        resolve(collection);
                    }
                }

                function promiseRemote(resolve, reject) {

                    var url = null;

                    switch (pnomenclador) {
                        case 'TIPO_PUBLICACION':
                            if (pid == null)
                                url = API_INFOGUIA + '/tiposPublicaciones/find/all';
                            else
                                url = API_INFOGUIA + '/tiposPublicaciones/find/' + pid;
                            break;
                        case 'ESTADO_PUBLICACION':
                            if (pid == null)
                                url = API_INFOGUIA + '/estadosPublicaciones/find/all';
                            else
                                url = API_INFOGUIA + '/estadosPublicaciones/find/' + pid;
                            break;
                        case 'GRUPO_CATEGORIA':
                            if (pid == null)
                                url = 'data/grupo_categoria.json';
                            else
                                url = null;
                            break;
                        default:
                            url = null;
                    }

                    if (!pnomenclador || url == null) {
                        reject('Debe indicar el parámetro "pnomenclador" de la función');
                    }
                    else {
                        $http({
                            method: 'GET',
                            url: url
                        }).then(function (response) {
                            resolve(response.data);
                        }, function (response) {
                            reject("Ha ocurrido un error cargando. " + pnomenclador + " => " + response.statusText);
                        });
                    }
                }

                return !premote ? $q(promise) : $q(promiseRemote);
            }
        };

        return service;
    }

    /*@ngInject*/
    function AppFileSvc($q, $http, $interpolate, API_INFOGUIA) {
        var service = {
            remove: function (pid) {

                var id = pid || null;
                var exp = $interpolate(API_INFOGUIA + '/archivos/delete/{{ id }}');
                var url = exp({id: id});

                function promise(resolve, reject) {

                    if (id != null) {
                        $http({
                            method: 'DELETE',
                            url: url
                        }).then(function (response) {
                            resolve(response.data);
                        }, function (response) {
                            reject("Ha ocurrido un error intentando eliminar el archivo. " + pid + " => " + response.statusText);
                        });
                    } else {
                        reject("Identificador de archivo no establecido.");
                    }
                }

                return $q(promise);
            }
        };

        return service;
    }

    /*@ngInject*/
    function AppSetting() {

        var settings = {
            noimage: 'web/assets/img/no-thumbnail.jpg',
            defaultUserAvatar: 'web/assets/img/default-user-avatar.png',
            noimageBig: 'web/assets/img/no-thumbnail-big.jpg'
        };

        return settings;
    }

    /*@ngInject*/
    function CategoriaSvc($http, $q, API_INFOGUIA) {

        var API_MODEL_URL = API_INFOGUIA + '/categorias/';

        var service = {
            query: function () {

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'find/all';

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de categorias...");
                    }
                }

                return $q(promise);
            },
            getById: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'find/' + modelID;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo categoria...");
                    }
                }

                return $q(promise);
            }
        };

        return service;

    }

    /*@ngInject*/
    function PublicacionSvc($http, $q, API_INFOGUIA) {

        var API_MODEL_URL = API_INFOGUIA + '/publicaciones/';
        var API_REST_TIPO = API_INFOGUIA + '/tiposPublicaciones/';

        var service = {
            // GET /publicaciones/find/all
            query: function () {

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'find/all';

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de categorias...");
                    }
                }

                return $q(promise);
            },
            //GET /publicaciones/findByTipo/{tipoPublicacion}
            getByTipo: function (id) {

                var tipoID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'findByTipo/' + tipoID;

                    if (tipoID == null) {
                        reject("Indique el tipo de publicacion");
                        return;
                    }

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo publicaciones...");
                    }
                }

                return $q(promise);
            },
            getByCliente: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'findByCliente/' + modelID;

                    if (!modelID) {
                        reject("Id de cliente no establecido...");
                        return;
                    }

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo publicación...");
                    }
                }

                return $q(promise);
            },
            getTipoPublicacion: function () {

                function promise(resolve, reject) {

                    var url = API_REST_TIPO + 'find/all';

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo tipo de publicación publicación...");
                    }
                }

                return $q(promise);
            },
            findByCodigo: function (codigo) {

                var key = codigo || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'findByCodigo/' + key;

                    if (!key) {
                        reject("Código de tipo publicación no establecido...");
                        return;
                    }

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo publicaciones...");
                    }
                }

                return $q(promise);
            },
            createWish: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'listaDeseos/' + modelID;

                    if (!modelID) {
                        reject("Id de usuario no establecido...");
                        return;
                    }

                    $http({
                        method: "POST",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err agregando a favorito...");
                    }
                }

                return $q(promise);
            },
            deleteWish: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL_URL + 'listaDeseos/' + modelID;

                    if (!modelID) {
                        reject("Id de usuario no establecido...");
                        return;
                    }

                    $http({
                        method: "DELETE",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err eliminando de lista de desos...");
                    }
                }

                return $q(promise);
            }
        };

        return service;

    }

    /*@ngInject*/
    function ClienteSvc($http, $q, API_INFOGUIA, AppSetting) {

        var API_CLIENTE = API_INFOGUIA + '/clientes/';
        var API_CLIENTE_SEARCH = API_INFOGUIA + '/search/clientes';

        var service = {
            query: function () {

                function promise(resolve, reject) {

                    var url = API_CLIENTE + 'find/all';

                    var requestParams = {
                        method: "GET",
                        url: url
                    };

                    $http(requestParams).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var data = response.data;
                        data.map(function (item, index) {
                            item['imagen'] = AppSetting.noimage;
                        });
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de clientes...");
                    }
                }

                return $q(promise);
            },
            get: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_CLIENTE + 'find/' + modelID;

                    var requestParams = {
                        method: "GET",
                        url: url
                    };

                    $http(requestParams).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo cliente...");
                    }
                }

                return $q(promise);
            },
            getById: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_CLIENTE + 'find/' + modelID;

                    var requestParams = {
                        method: "GET",
                        url: url
                    };

                    $http(requestParams).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo cliente...");
                    }
                }

                return $q(promise);
            },
            search: function (q) {

                var query = q || null;

                function promise(resolve, reject) {

                    if (query == null) {
                        reject("Debe indicar parámetros de búsqueda");
                        return;
                    }

                    var url = API_CLIENTE_SEARCH;

                    var data = {
                        query: q.query || null,
                        categoryId: q.categoryId || null,
                        page: q.page || 0,
                        cityId: q.idCiudad || null
                    };

                    console.log('QRY DATA:', data);

                    var requestParams = {
                        method: "POST",
                        url: url,
                        data: data
                    };

                    $http(requestParams).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var data = response.data;
                        console.log(data);

                        if (data && data.result && data.result.length > 0)
                            data.result.map(function (item, index) {
                                if (!angular.isDefined(item.sucursalDto.archivos) || item.sucursalDto.archivos == null || item.sucursalDto.archivos.length == 0)
                                    item['imagen'] = AppSetting.noimage;
                                else
                                    item['imagen'] = item.sucursalDto.archivos[0]['url'];
                            });
                        resolve(data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de clientes...");
                    }
                }

                return $q(promise);
            }
        };

        return service;

    }

    /*@ngInject*/
    function SucursalSvc($http, $q, API_INFOGUIA, $filter, AppSetting) {

        var API_SUCURSAL = API_INFOGUIA + '/sucursales/';

        var service = {
            query: function () {

                function promise(resolve, reject) {

                    var url = API_SUCURSAL + 'find/all';

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo listado de clientes...");
                    }
                }

                return $q(promise);
            },
            get: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_SUCURSAL + 'find/' + modelID;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var value = response.data;
                        //
                        //coordenadas
                        var coordsFiltered = $filter('coordFilter')(value['coordenadas']);
                        value['coordenadas'] = coordsFiltered;
                        //
                        //imagen
                        if (!angular.isDefined(value.archivos) || value.archivos == null || value.archivos.length == 0)
                            value['imagen'] = AppSetting.noimage;
                        else
                            value['imagen'] = value.archivos[0]['url'];

                        resolve(value);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo sucursal...");
                    }
                }

                return $q(promise);
            },
            getByCliente: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_SUCURSAL + 'findByCliente/' + modelID;

                    if (!modelID) {
                        reject("Id de cliente no establecido...");
                        return;
                    }

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var data = response.data;
                        data.map(function (value, i) {
                            //
                            //coordenadas
                            var coordsFiltered = $filter('coordFilter')(value['coordenadas']);
                            value['coordenadas'] = coordsFiltered;
                            //
                            //imagen
                            if (!angular.isDefined(value.archivos) || value.archivos == null || value.archivos.length == 0)
                                value['imagen'] = AppSetting.noimage;
                            else
                                value['imagen'] = value.archivos[0]['url'];
                        });
                        resolve(data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo sucursal...");
                    }
                }

                return $q(promise);
            },
            createFavourite: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_SUCURSAL + 'favoritos/' + modelID;

                    if (!modelID) {
                        reject("Id de usuario no establecido...");
                        return;
                    }

                    $http({
                        method: "POST",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err agregando a favorito...");
                    }
                }

                return $q(promise);
            },
            deleteFavourite: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_SUCURSAL + 'favoritos/' + modelID;

                    if (!modelID) {
                        reject("Id de usuario no establecido...");
                        return;
                    }

                    $http({
                        method: 'DELETE',
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response + ". Err eliminando de favorito...");
                    }
                }

                return $q(promise);
            }
        };

        return service;

    }

    /*@ngInject*/
    function ClienteCategoriaSvc($http, $q, $filter, API_INFOGUIA, CategoriaSvc) {

        var API_MODEL = API_INFOGUIA + '/clienteCategorias/';

        var service = {
            getByClienteId: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL + 'findByCliente/' + modelID;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err getByClienteId...");
                    }
                }

                return $q(promise);
            },
            getByCategoriaId: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_MODEL + 'findByCategoria/' + modelID;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err getByCategoriaId...");
                    }
                }

                return $q(promise);
            },
            getByClienteIdResume: function (id) {

                function promise(resolve, reject) {

                    service.getByClienteId(id).then(function (data) {
                        return $q(function (res, rej) {
                            res(data);
                        });
                    }).then(function (data) {

                        CategoriaSvc.query().then(function (rcategories) {
                            var categories = rcategories;
                            var ccliente = data;

                            var arrCategories = [];

                            for (var i = 0; i < categories.length; i++) {

                                var found = false;
                                var cat = categories[i];

                                for (var j = 0; j < ccliente.length; j++) {
                                    var catCliente = ccliente[j];

                                    var catClienteID = catCliente['categoriaDto']['id'];
                                    var catID = cat['id'];

                                    if (catClienteID === catID)
                                        found = true;
                                }

                                if (!found) {
                                    arrCategories.push(cat);
                                }
                            }

                            resolve({categorias: arrCategories, clienteCategorias: ccliente});

                        });

                    }, function (err) {
                        reject(err);
                    });
                }

                return $q(promise);
            }
        };

        return service;

    }

    /*@ngInject*/
    function AccountSvc($http, $q, API_INFOGUIA, $filter, AppSetting, AuthSvc, Upload) {

        var API_PROFILE = API_INFOGUIA + '/usuarioPerfiles/';

        var service = {
            get: function () {

                function promise(resolve, reject) {

                    var isAuth = AuthSvc.isAuth();
                    if (!isAuth) {
                        reject('Debe autenticarse para visualizar su perfil');
                        return;
                    }
                    var user = AuthSvc.appUser();
                    var url = API_PROFILE + 'findByUser/' + user.uid;
                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var data = response.data;
                        if (data && data.archivos != null && data.archivos.length > 0)
                            data['avatar'] = data.archivos[data.archivos.length - 1]['url'];
                        else {
                            data['avatar'] = AppSetting.defaultUserAvatar;
                        }

                        resolve(data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo perfil de usuario...");
                    }
                }

                return $q(promise);
            },
            update: function (model) {

                function promise(resolve, reject) {

                    var url = API_PROFILE + 'update';

                    $http({
                        method: 'PUT',
                        url: url,
                        data: model
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject("Ha ocurrido un error mientras se actualizaba su cuenta. " + response.statusText);
                    }
                }

                return $q(promise);
            },
            uploadImage: function (file, pid) {

                var id = pid || null;

                function promise(resolve, reject) {

                    var url = API_PROFILE + 'upload/' + id;

                    if (file && id !== null) {

                        Upload.upload({
                            url: url,
                            data: {fileData: file, id: id}
                        }).then(function (resp) {
                            console.log('Success ' + resp.config.data.fileData.name + 'uploaded. Response: ' + resp.data);
                            resolve(resp.data.archivosDetDto[0]);
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                            reject('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.fileData.name);
                        });
                    } else {
                        reject("Error intentando subir la image de la perfil de usuario.");
                    }
                }

                return $q(promise);
            },
            myWishes: function () {

                function promise(resolve, reject) {

                    var isAuth = AuthSvc.isAuth();
                    if (!isAuth) {
                        reject('Debe autenticarse para visualizar su lista de deseos');
                        return;
                    }
                    var user = AuthSvc.appUser();
                    var url = API_PROFILE + 'wishes/' + user.uid;

                    resolve([]);

                    return;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo lista de deseos...");
                    }
                }

                return $q(promise);
            },
            myFavourites: function () {

                function promise(resolve, reject) {

                    var isAuth = AuthSvc.isAuth();
                    if (!isAuth) {
                        reject('Debe autenticarse para visualizar su lista de favoritos');
                        return;
                    }
                    var user = AuthSvc.appUser();
                    var url = API_PROFILE + 'favourites/' + user.uid;

                    resolve([]);

                    return;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo lista de favoritos...");
                    }
                }

                return $q(promise);
            },
        };

        return service;

    }

    /*@ngInject*/
    function ValoracionSvc($http, $q, API_INFOGUIA, $filter, AppSetting, ProductSvc) {

        var API_VALORACION = API_INFOGUIA + '/valoraciones';

        var service = {
            create: function (model) {

                function promise(resolve, reject) {

                    var url = API_VALORACION + '/add';

                    $http({
                        method: "POST",
                        url: url,
                        data: model
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error creando valoración...");
                    }
                }

                return $q(promise);
            },
            findBySucursal: function (id) {

                //return ProductSvc.getComments(1);

                function promise(resolve, reject) {

                    var url = API_VALORACION + '/findBySucursal/' + id;

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {

                        var data = response.data;

                        var comments = [];

                        if (data && data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                var it_valoraciones = data[i]['sucursalValoracionesDetDto'];
                                for (var j = 0; j < it_valoraciones.length; j++) {

                                    var it_valoracion = it_valoraciones[j];

                                    var comment = {
                                        id: it_valoracion.id,
                                        usuario: it_valoracion.usuarioDto.username,
                                        usuarioId: it_valoracion.usuarioDto.id,
                                        usuarioEmail: it_valoracion.usuarioDto.email,
                                        comentario: it_valoracion.mensaje,
                                        fecha: $filter('date')(new Date()),
                                        rating: it_valoracion.puntaje ? parseInt(it_valoracion.puntaje) : 1
                                    };

                                    comments.push(comment);
                                }
                            }
                        }

                        resolve(comments);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Error obteniendo valoraciones por Sucursal...");
                    }
                }

                return $q(promise);
            },
            findByCliente: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_VALORACION + '/findByCliente/' + id;

                    if (!modelID) {
                        reject("Id de cliente no establecido...");
                        return;
                    }

                    $http({
                        method: "GET",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        var data = response.data;
                        resolve(data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err obteniendo valoración por cliente...");
                    }
                }

                return $q(promise);
            },
            delete: function (id) {

                var modelID = id || null;

                function promise(resolve, reject) {

                    var url = API_VALORACION + '/delete/' + modelID;

                    $http({
                        method: "DELETE",
                        url: url
                    }).then(successResponse, errorResponse);

                    function successResponse(response) {
                        resolve(response.data);
                    }

                    function errorResponse(response) {
                        reject(response.statusText + " Err eliminando valoración...");
                    }
                }

                return $q(promise);
            }
        };

        return service;

    }

})(jQuery);    
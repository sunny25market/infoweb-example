(function ($) {

    "use strict";
    angular.module('app')
            .directive('a', A)
            .directive('mcuiPostCategories', McuiPostCategories)
            .directive('mcuiBroadcastSubcategory', McuiBroadcastSubcategory)
            .directive('soSingleImagePicker', SoSingleImagePicker)
            .directive('mcuiAccountImagepicker', McuiAccountImagepicker)
            .directive('mcuiOnlyNumbers', McuiOnlyNumbers)
            .directive('mcuiFavourite', McuiFavourite)
            .directive('mcuiWish', McuiWish)
            .directive('mcuiBgimageCliente', McuiBgimageCliente)
            .directive('mcuiBgimageSucursal', McuiBgimageSucursal)
            .directive('mcuiThumbCliente', McuiThumbCliente)
            .directive('mcuiThumbSucursal', McuiThumbSucursal)
            .directive('mcuiThumbBg', McuiThumbBg)
            .directive('mcuiBarRating', McuiBarRating)
            .directive('mcuiCheck', McuiCheck)
            .directive('mcuiCountertp', McuiCountertp);

    /*@ngInject*/
    function A() {
        return {
            restrict: 'E',
            scope: {linkDisabled: '=', linkBlocked: '='},
            link: function (scope, element, attrs) {

                if (scope.linkDisabled === true) {
                    $(element).addClass('link-disabled');
                }

                if (scope.linkBlocked === true) {
                    $(element).addClass('link-blocked');
                }

                element.on('click', function (e) {
                    if (attrs.ngClick || attrs.href == '' || attrs.href == '#' || /^#tab/.test(attrs.href)) {
                        e.preventDefault();
                    }
                });
            }
        };
    }

    /*@ngInject*/
    function McuiPostCategories($interpolate, $compile, $rootScope) {
        var directive = {
            restrict: 'EA',
            link: function (scope, element, attrs) {

                var getTemplate = function (categories) {
                    //param
                    var srefInterpolate = $interpolate('product.searchbysubcategory({subcategoryid:{{ id }}})');
                    //sref, title
                    var linkTagInterpolate = $interpolate('<a href="#" ng-click="clickOncategory({{ id }})" title="{{ title }}"><i class="fa fa-tag"></i> {{ title }}</a>');
                    var tmpl = '<div class="post-categories">';
                    angular.forEach(categories, function (value, index) {
                        tmpl += linkTagInterpolate({id: value.id, title: value.descripcion});
                        if (categories.length - 1 != index)
                            tmpl += ', ';
                    });
                    tmpl += '</div>';
                    return tmpl;
                };
                scope.clickOncategory = function (id) {
                    $rootScope.$broadcast('post:filterby:subcategory', id);
                };
                var tmpl = getTemplate(angular.copy(scope.categories));
                element.html(tmpl);
                $compile(element.contents())(scope);
            },
            scope: {
                categories: '='
            }
        };
        return directive;
    }

    /*@ngInject*/
    function McuiBroadcastSubcategory($interpolate, $compile, $rootScope) {
        var directive = {
            restrict: 'A',
            link: function (scope, element, attrs) {

                element.on('click', function (evt) {
                    var catId = attrs.mcuiBroadcastSubcategory || null;
                    if (catId != null)
                        $rootScope.$broadcast('post:filterby:subcategory', catId);
                });
            },
            scope: false
        };
        return directive;
    }

    /*@ngInject*/
    function SoSingleImagePicker($timeout) {

        /*@ngInject*/
        function SingleImagePickerController($scope) {
            var vm = this;
            vm.fn = {
                changeFilesSelect: changeFilesSelect,
                resetFile: resetFile
            };
            vm.init = Init;
            Init();
            function Init() {
            }

            function changeFilesSelect($file) {
                if (angular.isUndefined($file) || !$file)
                    return;
                $scope.file = $file;
            }

            function resetFile($e) {
                $e.preventDefault();
                $e.stopPropagation();
                $scope.$broadcast('sip:reset', {source: $scope.source});
            }
        }

        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                scope.showFileInput = (attrs.fileInput) ? scope.$eval(attrs.fileInput) : true;
                scope.$on('sip:reset', function (event, message) {

                    if (!scope.file)
                        return;
                    scope.file = null;
                    $timeout(function () {
                        element.find('img')
                                .prop('src', scope.source);
                    }, 100);
                });
                attrs.$observe('source', function () {
                    if (attrs.source) {
                        $timeout(function () {
                            scope.source = attrs.source;
                            element.find('img')
                                    .prop('src', scope.source);
                        }, 100);
                    }
                });
            },
            templateUrl: function () {
                return 'web/views/components/cmp.singleimagepicker.html';
            },
            controller: SingleImagePickerController,
            controllerAs: 'vm',
            scope: {
                file: "=",
                source: "@"
            }
        }
    }

    /*@ngInject*/
    function McuiAccountImagepicker($timeout) {

        /*@ngInject*/
        function SingleImagePickerController($scope) {
            var vm = this;
            vm.fn = {
                changeFilesSelect: changeFilesSelect,
                resetFile: resetFile
            };
            vm.init = Init;
            Init();
            function Init() {
            }

            function changeFilesSelect($file) {
                if (angular.isUndefined($file) || !$file)
                    return;
                $scope.file = $file;
            }

            function resetFile($e) {
                $e.preventDefault();
                $e.stopPropagation();
                $scope.$broadcast('sip:reset', {source: $scope.source});
            }
        }

        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                scope.showFileInput = (attrs.fileInput) ? scope.$eval(attrs.fileInput) : true;
                scope.$on('sip:reset', function (event, message) {

                    if (!scope.file)
                        return;
                    scope.file = null;
                    $timeout(function () {
                        element.find('img')
                                .prop('src', scope.source);
                    }, 100);
                });
                attrs.$observe('source', function () {
                    if (attrs.source) {
                        $timeout(function () {
                            scope.source = attrs.source;
                            element.find('img')
                                    .prop('src', scope.source);
                        }, 100);
                    }
                });
            },
            templateUrl: function () {
                return 'web/views/components/cmp.account.imagepicker.html';
            },
            controller: SingleImagePickerController,
            controllerAs: 'vm',
            scope: {
                file: "=",
                source: "@"
            }
        }
    }

    /*@ngInject*/
    function McuiOnlyNumbers() {

        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {

                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');
                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }

                ngModelCtrl.$parsers.push(fromUser);
            }
        }
    }

    /*@ngInject*/
    function McuiFavourite($timeout, $rootScope, SucursalSvc) {

        var directive = {
            restrict: 'EA',
            transclude: true,
            template: function (element, attrs) {
                var template = '<button ng-if="!ishidden" title="{{ title }}" class="btn-add-to-my-list btn-favourite"><i class="fa" ng-class="checked == true ? \'fa-star\':  \'fa-star-o\' "></i><span class="btn-content" ng-transclude></span></button';
                return template;
            },
            link: function (scope, element, attrs) {

                scope.title = attrs.title || 'Agregar/Eliminar de mis favoritos';

                scope.ishidden = $rootScope.$appUser != null ? false : true;

                scope.$on('auth:login', function (evt, val) {
                    if (val && val != null)
                        scope.ishidden = false;
                })

                element.on('click', function () {
                    var isChecked = scope.checked;
                    var id = attrs.itemId || null;

                    if (!id || id == null)
                        return;

                    if (!isChecked) {
                        console.log('add to favourite...');
                        SucursalSvc.createFavourite(id).then(function (response) {
                            console.log(response);
                            $timeout(function () {
                                scope.checked = true;
                            })
                        }, function (err) {
                            console.log(err);
                        })
                    } else {
                        console.log('delete to favourite...');
                        SucursalSvc.deleteFavourite(id).then(function (response) {
                            $timeout(function () {
                                scope.checked = false;
                            })
                        }, function (err) {
                            console.log(err);
                        })
                    }
                })
            },
            scope: {
                checked: '='
            }
        };
        return directive;
    }

    /*@ngInject*/
    function McuiWish($timeout, $rootScope, PublicacionSvc) {

        var directive = {
            restrict: 'EA',
            transclude: true,
            template: function (element, attrs) {
                var template = '<button ng-if="!ishidden" title="{{ title }}" class="btn-add-to-my-list btn-wish"><i class="fa" ng-class="checked == true ? \'fa-heart\':  \'fa-heart-o\' "></i><span class="btn-content" ng-transclude></span></button';
                return template;
            },
            link: function (scope, element, attrs) {

                scope.title = attrs.title || 'Agregar/Eliminar de mi lista de deseos';

                scope.ishidden = $rootScope.$appUser != null ? false : true;

                scope.$on('auth:login', function (evt, val) {
                    if (val && val != null)
                        scope.ishidden = false;
                })

                element.on('click', function () {

                    var isChecked = scope.checked;
                    var id = attrs.itemId || null;

                    if (!id || id == null)
                        return;

                    if (!isChecked) {
                        console.log('add to wish list...');
                        PublicacionSvc.createWish(id).then(function (response) {
                            console.log(response);
                            $timeout(function () {
                                scope.checked = true;
                            })
                        }, function (err) {
                            console.log(err);
                        })
                    } else {
                        console.log('delete to wish...');
                        PublicacionSvc.deleteWish(id).then(function (response) {
                            console.log(response);
                            $timeout(function () {
                                scope.checked = false;
                            })
                        }, function (err) {
                            console.log(err);
                        })
                    }
                })
            },
            scope: {
                checked: '='
            }
        };

        return directive;
    }

    /*@ngInject*/
    function McuiBgimageCliente($interpolate, $timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch('cliente', function (newValue, oldValue) {
                    if (newValue === oldValue)
                        return;
                    var urlImage = "https://dummyimage.com/1200x300.jpg/ECECEC";
                    try {
                        urlImage = newValue['archivos'][newValue['archivos'].length - 1]['url'];
                    } catch (e) {
                        console.log('Error cargando imagen del cliente: ', e);
                    }
                    var bgInterpolate = $interpolate('background: url({{ url }}) center top no-repeat;');
                    var bgCss = bgInterpolate({url: urlImage});

                    $timeout(function () {
                        element.prop('style', bgCss);
                    });
                });
            },
            scope: {
                cliente: "="
            }
        };
    }

    /*@ngInject*/
    function McuiBgimageSucursal($interpolate, $timeout, AppSetting) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch('sucursal', function (newValue, oldValue) {

                    if (typeof newValue == 'undefined')
                        return;

                    var urlImage = AppSetting.noimageBig;
                    try {
                        if (newValue['archivos'] && newValue['archivos'] != null && newValue['archivos'].length > 0)
                            urlImage = newValue['archivos'][newValue['archivos'].length - 1]['url'];
                    } catch (e) {
                        console.log('Error cargando imagen del cliente: ', e);
                    }
                    var interpolateString = 'background-image: url({{ url }});';
                    interpolateString += ' background-repeat: no-repeat;';
                    interpolateString += ' background-position: center top;';
                    interpolateString += ' background-attachment: scroll;';
                    interpolateString += ' background-size: cover;';

                    var bgInterpolate = $interpolate(interpolateString);
                    var bgCss = bgInterpolate({url: urlImage});

                    $timeout(function () {
                        element.prop('style', bgCss);
                    });
                });
            },
            scope: {
                sucursal: "="
            }
        };
    }

    /*@ngInject*/
    function McuiThumbCliente($timeout, AppSetting) {
        return {
            restrict: 'EA',
            replace: true,
            template: function () {

                var tmpl = '';
                tmpl += '<div class="thumbnail">';
                tmpl += '   <img ng-src="{{ imageUrl }}" class="" title="{{ cliente.nombreCompleto }}" />';
                tmpl += '</div>';

                return tmpl;
            },
            link: function (scope, element, attrs) {
                scope.$watch('cliente', function (newValue, oldValue) {
                    if (newValue === oldValue)
                        return;
                    var urlImage = AppSetting.noimage;//"https://dummyimage.com/1200x300.jpg/ECECEC";
                    try {
                        if (newValue['archivos'] && newValue['archivos'] != null && newValue['archivos'].length > 0)
                            urlImage = newValue['archivos'][newValue['archivos'].length - 1]['url'];
                    } catch (e) {
                        console.log('Error cargando imagen del cliente: ', e);
                    }

                    $timeout(function () {
                        scope.imageUrl = urlImage;
                    });
                });
            },
            scope: {
                cliente: "="
            }
        };
    }

    /*@ngInject*/
    function McuiThumbSucursal($interpolate, $timeout, AppSetting) {
        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                scope.$watch('sucursal', function (newValue, oldValue) {

                    var urlImage = AppSetting.noimage;
                    try {
                        urlImage = newValue.imagen;
                    } catch (e) {
                        console.log('Error cargando imagen del cliente: ', e);
                    }

                    var interpolateString = 'background-image: url({{ url }});';
                    interpolateString += ' background-repeat: no-repeat;';
                    interpolateString += ' background-position: center top;';
                    interpolateString += ' background-attachment: scroll;';
                    interpolateString += ' background-size: cover;';

                    var bgInterpolate = $interpolate(interpolateString);
                    var bgCss = bgInterpolate({url: urlImage});

                    $timeout(function () {
                        element.find('.thumb').prop('style', bgCss);
                    });
                });
            },
            scope: {
                sucursal: "="
            }
        };
    }

    /*@ngInject*/
    function McuiThumbBg($interpolate, $timeout, AppSetting) {
        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {

                var urlImage = attrs.mcuiThumbBg || AppSetting.noimage;

                //scope.$watch('McuiThumbBg', function (newValue, oldValue) {
                var interpolateString = 'background-image: url({{ url }});';
                interpolateString += ' background-repeat: no-repeat;';
                interpolateString += ' background-position: center top;';
                interpolateString += ' background-attachment: scroll;';
                interpolateString += ' background-size: cover;';

                var bgInterpolate = $interpolate(interpolateString);
                var bgCss = bgInterpolate({url: urlImage});

                $timeout(function () {
                    element.prop('style', bgCss);
                });
                //});
            },
            scope: {
                sucursal: "="
            }
        };
    }

    /*@ngInject*/
    function McuiBarRating($http, $timeout) {

        function McuiBarRatingLink(scope, element, attrs) {

            var attrsSettings = {};

            var settingsAvailable = [
                'theme',
                'initialRating',
                'allowEmpty',
                'emptyValue',
                'showValues',
                'showSelectedRating',
                'deselectable',
                'reverse',
                'readonly',
                'fastClicks',
                'hoverState',
                'silent'
            ];

            for (var att in attrs) {
                var index = settingsAvailable.indexOf(att);
                if (index != -1)
                    attrsSettings[settingsAvailable[index]] = scope.$eval(attrs[att]);
            }

            var cssClass = attrs.cssClass || '';

            var defaultSettings = {
                //Defines a theme.
                theme: 'fontawesome-stars',
                //Defines initial rating.
                initialRating: 1,
                //If set to true, users will be able to submit empty ratings.
                allowEmpty: null,
                //Defines a value that will be considered empty. It is unlikely you will need to modify this setting.
                emptyValue: '',
                //If set to true, rating values will be displayed on the bars.
                showValues: false,
                //If set to true, user selected rating will be displayed next to the widget.
                showSelectedRating: true,
                //If set to true, users will be able to deselect ratings.
                deselectable: true,
                //If set to true, the ratings will be reversed.
                reverse: false,
                //If set to true, the ratings will be read-only.
                readonly: false,
                //Remove 300ms click delay on touch devices.
                fastClicks: true,
                //Change state on hover.
                hoverState: true,
                //Supress callbacks when controlling ratings programatically.
                silent: false
            };

            var defaultEvents = {
                onSelect: function (value, text, event) {
                    if (typeof (event) !== 'undefined') {
                        // rating was selected by a user
                        if (typeof scope.onSelect == 'function')
                            scope.onSelect({value: value, text: text, event: event});
                    } else {
                        // rating was selected programmatically
                        // by calling `set` method
                    }
                },
                onClear: function (value, text) {
                    if (typeof scope.onClear == 'function')
                        scope.onClear({value: value, text: text});
                },
                onDestroy: function (value, text) {
                    if (typeof scope.onDestroy == 'function')
                        scope.onDestroy({value: value, text: text});
                }
            };

            var settings = angular.extend({}, defaultSettings, attrsSettings, defaultEvents);

            scope.$watch('ngModel', function (newValue, oldValue) {
                if (angular.isDefined(newValue) && newValue != null) {
                    $timeout(function () {
                        element.barrating('set', parseInt(newValue, 10));
                    })
                }
            });

            $timeout(function () {
                element.barrating(settings);
                element.closest('.br-wrapper').addClass(cssClass);
            });
        }

        function McuiTemplateFunc(element, attrs) {
            var tmpl = '';
            tmpl += '<select style="display:none">';
            tmpl += '   <option value="1">1</option>';
            tmpl += '   <option value="2">2</option>';
            tmpl += '   <option value="3">3</option>';
            tmpl += '   <option value="4">4</option>';
            tmpl += '   <option value="5">5</option>';
            tmpl += '</select>';

            return tmpl;
        }

        return {
            restrict: 'E',
            replace: true,
            link: McuiBarRatingLink,
            template: McuiTemplateFunc,
            scope: {
                ngModel: '=?',
                onSelect: '&',
                onClear: '&',
                onDestroy: '&'
            }
        };
    }

    /*@ngInject*/
    function McuiCheck($http, $timeout, $rootScope) {

        var directive = {
            restrict: 'EA',
            transclude: true,
            template: function (element, attrs) {
                var template = '';
                template += '<button ng-click="fn.checkUncheck()" title="{{ title }}" class="btn-customized btn-check {{checkedCssClass}}">';
                template += '   <i class="fa fa-check-circle-o"></i>';
                template += '       <span class="" ng-if="checkByUser && !checkByAdmin">Usuario</span>';
                template += '       <span class="" ng-if="checkByAdmin">Infoguía</span>';
                template += '       <span class="" ng-if="!checkByUser && !checkByAdmin">';
                template += '           No Verificado';
                template += '        </span>';
                template += '   <span class="btn-content" ng-transclude></span>';
                template += '</button';
                return template;
            },
            link: function (scope, element, attrs) {

                scope.title = attrs.title || 'Marcar como verificado';

                scope.ishidden = $rootScope.$appUser != null ? false : true;

                scope.readonly = attrs.readonly ? true : false;

                scope.$on('auth:login', function (evt, val) {
                    if (val && val != null)
                        scope.ishidden = false;
                });

                scope.fn = {
                    checkUncheck: function () {

                        if (scope.ishidden || scope.readonly)
                            return;

                        $timeout(function () {
                            if (!scope.checkByAdmin) {
                                //var ischbuser = scope.checkByUser ? false : true;
                                scope.checkByUser = !scope.checkByUser;
                            }
                        })
                    }
                }

                scope.$watch('checkByAdmin', function (n, o) {
                    setCssBtnClass();
                })

                scope.$watch('checkByUser', function (n, o) {
                    setCssBtnClass();
                })

                setCssBtnClass();

                function setCssBtnClass() {
                    if (scope.checkByAdmin) {
                        scope.checkedCssClass = 'checked-by-admin';
                    } else if (scope.checkByUser) {
                        scope.checkedCssClass = 'checked-by-user';
                    } else {
                        scope.checkedCssClass = 'no-checked';
                    }
                }
            },
            scope: {
                checkByUser: '=',
                checkByAdmin: '='
            }
        };

        return directive;
    }

    /*@ngInject*/
    function McuiCountertp($timeout) {
        var directive = {
            restrict: 'EA',
            replace: true,
            template: function () {
                return '<span class="badge">{{ counter }}</span>';
            },
            link: function (scope, element, attrs) {
                var codigo = attrs.codigo || null;

                scope.$watch('collection', function (collection, oldvalue) {
                    var counter = 0;
                    if (collection && collection.length > 0) {
                        angular.forEach(collection, function (item) {
                            if (item.tipoPublicacionDto.codigo == codigo)
                                counter++;
                        });
                    }
                    $timeout(function () {
                        scope.counter = counter;
                    });
                });
            },
            scope: {
                collection: '='
            }
        };
        return directive;
    }

})(jQuery);    
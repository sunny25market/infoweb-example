(function () {

    "use strict";

    angular.module('app')
            .config(ConfigRoute)
            .constant('APP_ROOT', {
                base: 'web/',
                views: 'web/views/',
                layouts: 'web/views/layouts/'
            });

    /*@ngInject*/
    function ConfigRoute($urlRouterProvider, $stateProvider, APP_ROOT) {

        $urlRouterProvider
                .when("/", "/home")
                .when("", "/home")
                .otherwise("/404");

        $stateProvider
                .state('site', {
                    data: {},
                    views: {
                        '': {
                            templateUrl: APP_ROOT.layouts + '_site.html'
                        }
                    }
                })
                //
                //LAYOUTS
                .state('site.layout', {
                    abstract: true,
                    data: {},
                    views: {
                        'top_bg@site': {
                            templateUrl: APP_ROOT.views + 'components/top_bg.html',
                            controller: 'TopBgController as vm'
                        },
                        'header_top@site': {
                            templateUrl: APP_ROOT.views + 'components/header_top.html',
                            controller: 'HeaderTopController as vm'
                        },
                        'mega_nav@site': {
                            templateUrl: APP_ROOT.views + 'components/mega_nav.html',
                            controller: 'MegaNavController as vm'
                        },
                        'footer@site': {
                            templateUrl: APP_ROOT.views + 'components/footer.html',
                            controller: 'FooterController as vm'
                        }
                    }
                })
                .state('site.layout.home', {
                    abstract: true,
                    data: {},
                    views: {
                        'content@site': {
                            templateUrl: APP_ROOT.views + '/layouts/_home.html',
                            controller: 'HomeController as vm'
                        }
                    }
                })
                .state('site.layout.product', {
                    abstract: true,
                    data: {},
                    views: {
                        'content@site': {
                            templateUrl: APP_ROOT.views + '/layouts/_product.html',
                            controller: 'ProductController as vm'
                        }
                    }
                })
                .state('site.layout.category', {
                    abstract: true,
                    data: {},
                    views: {
                        'content@site': {
                            templateUrl: APP_ROOT.views + '/layouts/_category.html',
                            controller: 'CategoryLayoutController as vm'
                        }
                    }
                }) 
                .state('site.layout.account', {
                    abstract: true,
                    data: {},
                    views: {
                        'content@site': {
                            templateUrl: APP_ROOT.views + '/layouts/_account.html',
                            controller: 'AccountLayoutController as vm'
                        }
                    }
                })  
                //
                //404
                .state('404', {
                    url: "/404",
                    parent: 'init.layout',
                    data: {pageTitle: "404"},
                    views: {
                        'content@init': {
                            templateUrl: APP_ROOT.views + 'partials/error/404.html'
                        }
                    }
                })                
                //
                //HOME
                .state('home', {
                    url: "/home",
                    parent: 'site.layout.home',
                    data: {
                        pageTitle: "Inicio",
                        pageHeader: "Inicio",
                        pageDescription: "Página de inicio del sitio"
                    },
                    views: {
                        'banner@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/banner.html',
                            controller: 'BannerController as vm'
                        },
                        'map_places@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/map_places.html',
                            controller: 'MapPlacesController as vm'
                        },
                        'cate@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/cate.html',
                            controller: 'CateController as vm'
                        },
                        'cate_bottom@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/cate_bottom.html',
                            controller: 'CateBottomController as vm'
                        },
                        'wellcome@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/wellcome.html',
                            controller: 'WellcomeController as vm'
                        },
                        'banner_bottom1@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/banner_bottom1.html',
                            controller: 'BannerBottom1Controller as vm'
                        },
                        'banner_bottom@home.layout': {
                            templateUrl: APP_ROOT.views + 'components/banner_bottom.html',
                            controller: 'BannerBottomController as vm'
                        },
                        'categorias@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/categorias.html',
                            controller: 'CategoriasController as vm'
                        },
                        'novedades@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/novedades.html',
                            controller: 'NovedadesController as vm'
                        },
                        'promociones@site.layout.home': {
                            templateUrl: APP_ROOT.views + 'components/promociones.html',
                            controller: 'PromocionesController as vm'
                        }
                    }
                })
                //
                //PRODUCT
                .state('product', {
                    abstract: true,
                    url: "/product",
                    parent: 'site.layout.product',
                    data: {},
                    views: {}
                })
                .state('product.search', {
                    url: "/search",
                    parent: 'product',
                    data: {
                        pageTitle: "Producto",
                        pageHeader: "Producto",
                        pageDescription: "Página de búsqueda de productos"
                    },
                    views: {
                        'main_content@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/index.html'
                        },
                        'sidebar_left@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/sidebar.left.html'
                        },
                        'sidebar_right@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/sidebar.right.html'
                        }
                    }
                })
                .state('product.searchbysubcategory', {
                    url: "/category/{id}",
                    parent: 'product.search',
                    data: {
                        pageTitle: "Producto",
                        pageHeader: "Producto",
                        pageDescription: "Página de búsqueda de productos"
                    },
                    views: {
                        'main_content@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/index.html'
                        },
                        'sidebar_right@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/sidebar.right.html'
                        }
                    }
                })
                .state('product.searchbytext', {
                    url: "/{text}",
                    parent: 'product.search',
                    data: {
                        pageTitle: "Producto",
                        pageHeader: "Producto",
                        pageDescription: "Página de búsqueda de productos"
                    },
                    views: {
                        'main_content@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/index.html'
                        },
                        'sidebar_right@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/sidebar.right.html'
                        }
                    }
                })                
                .state('product.view', {
                    url: "/view/{id}",
                    parent: 'product',
                    data: {
                        pageTitle: "Producto",
                        pageHeader: "Producto",
                        pageDescription: "Página de producto",
                        single: true
                    },
                    views: {
                        'main_content@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/view.html',
                            controller: 'ProductViewController as vm'
                        }
                    }
                })
                .state('product.view_sucursal', {
                    url: "/view/{id}/{idsucursal}",
                    parent: 'product',
                    data: {
                        pageTitle: "Producto",
                        pageHeader: "Producto",
                        pageDescription: "Página de producto",
                        single: true
                    },
                    views: {
                        'main_content@site.layout.product': {
                            templateUrl: APP_ROOT.views + 'product/view.html',
                            controller: 'ProductViewController as vm'
                        }
                    }
                })  
                //
                //CATEGORY
                .state('category', {
                    abstract: true,
                    url: "/category",
                    parent: 'site.layout.category',
                    data: {},
                    views: {}
                })  
                .state('category.list', {
                    url: "/list",
                    parent: 'category',
                    data: {
                        pageTitle: "Categorías",
                        pageHeader: "Categorías",
                        pageDescription: "Listado de categorías",                     
                    },
                    views: {
                        'main_content@site.layout.category': {
                            templateUrl: APP_ROOT.views + 'category/list.html',
                            controller: 'CategoryListController as vm'
                        }
                    }
                })                
                //
                //ACCOUNT
                .state('account', {
                    abstract: true,
                    url: "/account",
                    parent: 'site.layout.account',
                    data: {},
                    views: {}
                })  
                .state('account.view', {
                    url: "/view",
                    parent: 'account',
                    data: {
                        pageTitle: "Mi Perfil",
                        pageHeader: "Mi Perfil",
                        pageDescription: "Mi Perfil",                     
                    },
                    views: {
                        'main_content@site.layout.account': {
                            templateUrl: APP_ROOT.views + 'account/view.html',
                            controller: 'AccountViewController as vm'
                        }
                    }
                })
                .state('account.edit', {
                    url: "/edit",
                    parent: 'account',
                    data: {
                        pageTitle: "Mi Perfil",
                        pageHeader: "Mi Perfil",
                        pageDescription: "Mi Perfil",                     
                    },
                    views: {
                        'main_content@site.layout.account': {
                            templateUrl: APP_ROOT.views + 'account/edit.html',
                            controller: 'AccountEditController as vm'
                        }
                    }
                })                
    }

})();
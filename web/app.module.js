'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
        .module('app', [
            'ngAnimate',
            'ngSanitize',
            'ngCookies',
            'ui.router',
            'ngMap',
            'LocalStorageModule',
            'ngFileUpload',
            'angular-flexslider',
            'mgcrea.ngStrap',
            'app.home',
            'app.product',
            'app.site',
            'app.category',
            'app.account'
        ]);

angular.module('app')
        .config(CompileProviderConfig)
        .config(TestingInterceptorConfig)
        .config(Others)
        .constant('APP_UAUTH', {key: 'uAuthInfoguiaWeb'})
        .run(AppRun);

/*@ngInject*/
function AppRun($rootScope, $filter, $state, $document, $location, $log, AppSetting, AuthSvc) {

    $rootScope.$currentDate = new Date();
    $rootScope.$state = $state;
    $rootScope.$location = $location;
    $rootScope.$appGmapref = 'https://maps.googleapis.com/maps/api/js';
    $rootScope.$appUser = AuthSvc.appUser();
    $rootScope.$settings = AppSetting;

    var _notAllow = [
        'account.view',
        'account.edit'
    ];

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

        if (!AuthSvc.isAuth() && _notAllow.indexOf(toState.name) !== -1) {
            event.preventDefault();
            $state.go("home");
        }
    });

    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        event.preventDefault();
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.previousStateName = fromState.name;
        $rootScope.previousStateParams = fromParams;
        $rootScope.currentStateParams = toParams;
        $rootScope.currentState = toState;
        $rootScope.pageTitle = toState.data.pageTitle;
    });

    $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
        console.log(unfoundState.to);
        console.log(unfoundState.toParams);
        console.log(unfoundState.options);
    });

}

/*@ngInject*/
function TestingInterceptorConfig($httpProvider) {

    $httpProvider.interceptors.push(['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    var token = localStorageService.get('bearer') || null;
                    if (token)
                        config.headers.Authorization = 'Bearer ' + token;
                    else
                        config.headers.Authorization = 'Bearer ' + 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0OTcyMTM3MjksInN1YiI6IjI3In0.O4EarFUTzmErPYGBay7s05ZFneeFzx5aLxLR5Gb9Gk4'

                    return config;
                },
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $location.path('/home');
                    }
                    return $q.reject(response);
                }
            };
        }]);
}

/*@ngInject*/
function CompileProviderConfig($compileProvider, ENV) {

    if (ENV === 'dev')
        $compileProvider.debugInfoEnabled(true);
    else
        $compileProvider.debugInfoEnabled(false);
}

/*@ngInject*/
function Others($httpProvider) {

    $httpProvider.defaults.headers.common = "Accept: application/json, text/plain, *﻿/﻿*";
}

